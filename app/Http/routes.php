<?php

$app->routeMiddleware([
    'log' => App\Http\Middleware\LogMiddleware::class,
    'tokenCheck' => App\Http\Middleware\TokenCheckMiddleware::class,
    'tokenWrite' => App\Http\Middleware\TokenWriteMiddleware::class,
]);

$app->post('/Account/Login', 'BrokerController@auth');
$app->get('/Data/ClientList', 'BrokerController@clientList');
$app->get('/Data/AccountList', 'BrokerController@accountList');
$app->get('/Data/InstrumentList', 'BrokerController@instrumentList');
$app->get('/Data/Instrument', 'BrokerController@instrument');
$app->post('/Trading/Do/NewOrderRequest', 'BrokerController@newOrderRequest');