<?php

namespace App\Http\Middleware;

use App\Resources\Messages;
use Closure;

class TokenCheckMiddleware
{
    const TOKEN_LIFE_TIME = 86400; //сутки

    public function handle($request, Closure $next)
    {
        $token_get = $request->input('token');
        $token_data = app('db')->select("SELECT * FROM tokens WHERE token = '". $token_get ."';");

        if (empty($token_data)) {
            $response = $next($request);
            $response->setContent(json_encode(['result' => 'error', 'message' => Messages::TOKEN_EXPIRED]));
            LogMiddleware::logError($request->route()[1]['uses'], 0, Messages::TOKEN_EXPIRED);

            return $response;
        } else {
            return $next($request);
        }
    }
}
