<?php

namespace App\Http\Middleware;

use Closure;

class TokenWriteMiddleware
{
    const TOKEN_LIFE_TIME = 86400; //сутки

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $data = $response->getData();

        if ($data->result == 'ok') {
            $token = $data->data->token;
            $expires = time() + self::TOKEN_LIFE_TIME;
            app('db')->insert("INSERT INTO tokens (token, expires) VALUES ('". $token ."', '". date('Y-m-d H:i:s', $expires) ."');");
        }

        return $response;
    }
}
