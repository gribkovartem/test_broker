<?php

namespace App\Http\Middleware;

use Closure;

class LogMiddleware
{
    public static function logError($route, $result, $message)
    {
        app('db')->insert("INSERT INTO logs (route, result, message) VALUES ('". $route ."', '". $result ."', '". $message ."')");
    }

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $data = $response->getData();
        $result = ($data->result == 'error') ? 0 : 1;
        $route = $request->route()[1]['uses'];

        if ($result === 0) {
            self::logError($route, $result, $data->message);
        }

        return $response;
    }
}
