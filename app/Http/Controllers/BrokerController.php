<?php

namespace App\Http\Controllers;

use App\Resources\Messages;
use Illuminate\Http\Request;

class BrokerController extends Controller
{
    //тестовые аккаунты
    const BROKER_FIRST = ['login' => 'first_broker', 'password' => '123123'];
    const BROKER_SECOND = ['login' => 'second_broker', 'password' => '321321'];

    public function __construct()
    {
        $this->middleware('tokenWrite', ['only' => [
            'auth',
        ]]);
        $this->middleware('tokenCheck', ['only' => [
            'clientList',
            'accountList',
            'instrumentList',
            'instrument',
            'newOrderRequest'
        ]]);
        $this->middleware('log');
    }

    /**
     * POST /Account/Login
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function auth(Request $request)
    {
        $json_data = json_decode($request->instance()->getContent());

        if (empty($json_data)) {
            return response()->json(['result' => 'error', 'message' => Messages::NO_DATA]);
        }

        if (self::BROKER_FIRST['login'] == $json_data->login && self::BROKER_FIRST['password'] == $json_data->password) {
            $token = md5($json_data->login . $json_data->password . time());
            return response()->json(['result' => 'ok', 'data' => ['token' => $token]]);
        } else if (self::BROKER_FIRST['login'] != $json_data->login && self::BROKER_FIRST['password'] == $json_data->password) {
            return response()->json(['result' => 'error', 'message' => Messages::INCORRECT_LOGIN]);
        } else if (self::BROKER_FIRST['login'] == $json_data->login && self::BROKER_FIRST['password'] != $json_data->password) {
            return response()->json(['result' => 'error', 'message' => Messages::INCORRECT_PASSWORD]);
        } else {
            return response()->json(['result' => 'error', 'message' => Messages::INCORRECT_DATA]);
        }
    }

    /**
     * GET /Data/ClientList
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clientList(Request $request)
    {
        $timestamp = $request->input('ts');

        if (!isset($timestamp)) {
            return response()->json(['result' => 'error', 'message' => Messages::NO_TIMESTAMP]);
        }

        $clients = json_decode(file_get_contents(__DIR__ . '/../../../resources/tdata/clients.json'));
        $max_ts = json_decode(file_get_contents(__DIR__ . '/../../../resources/tdata/max_client_ts.json'))->ts;

        if ($timestamp > $max_ts) {
            $new_max_ts = '{"ts": '. $timestamp .'}';
            file_put_contents(__DIR__ . '/../../../resources/tdata/max_client_ts.json', $new_max_ts);
        }

        if ($timestamp === 0) {
            $data = $clients;
        } else {
            $data = [];
            foreach ($clients as $client) {
                if ($client->ts > $timestamp) {
                    $data[] = $client;
                }
            }
        }

        return response()->json(['result' => 'ok', 'data' => $data]);
    }

    /**
     * GET /Data/AccountList
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accountList(Request $request)
    {
        $client_id = $request->input('ClientId');
        $accounts = json_decode(file_get_contents(__DIR__ . '/../../../resources/tdata/accounts.json'));

        if (!isset($client_id)) {
            return response()->json(['result' => 'error', 'message' => Messages::NO_CLIENT_ID]);
        }

        if (isset($accounts->{$client_id})) {
            return response()->json(['result' => 'ok', 'data' => $accounts->{$client_id}]);
        } else {
            return response()->json(['result' => 'error', 'message' => Messages::NOT_FOUND_CLIENT_ID]);
        }
    }

    /**
     * GET /Data/InstrumentList
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function instrumentList()
    {
        $instruments = json_decode(file_get_contents(__DIR__ . '/../../../resources/tdata/instruments.json'));

        return response()->json(['result' => 'ok', 'data' => $instruments]);
    }

    /**
     * GET /Data/Instrument
     * !!!!!!! Передается два параметра InstrumentId(обязаельный) и Ticker, вот Ticker походу дела не нужный
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function instrument(Request $request)
    {
        $instrument_id = $request->input('InstrumentId');
        $ticker = $request->input('Ticker');

        if (!isset($instrument_id)) {
            return response()->json(['result' => 'error', 'message' => Messages::NO_INSTRUMENT_ID]);
        }

        $instruments = json_decode(file_get_contents(__DIR__ . '/../../../resources/tdata/instrument.json'));

        foreach ($instruments as $instrument) {
            if ($instrument->InstrumentId == $instrument_id && !isset($ticker)) {
                return response()->json(['result' => 'ok', 'data' => $instrument]);
            } else if ($instrument->Ticker == $ticker && $instrument->InstrumentId == $instrument_id) {
                return response()->json(['result' => 'ok', 'data' => $instrument]);
            }
        }

        return response()->json(['result' => 'error', 'message' => Messages::NOT_FOUND_INSTRUMENT_ID]);
    }

    public function newOrderRequest(Request $request)
    {
        $json_data = json_decode($request->instance()->getContent());
        $required_vars = ['ClientId', 'AccountId', 'BS', 'Type', 'Amount', 'Price', 'InstrumentId'];

        foreach ($json_data as $key => $json_data_item) {

        }

        $result = [
            'OrderRequestId' => 1,
            'OrderId' => 1,
            'Agreement' => '',
            'Date' => '',
            'Term' => '',
            'BS' => '',
            'Issuer.Name' => '',
            'Instrument.Kind' => '',
            'Amount' => '',
            'Currency.Name' => '',
            'Price' => '',
            'Exchange.Name' => ''
        ];

        return response()->json(['result' => 'ok', 'data' => $result]);
    }
}
