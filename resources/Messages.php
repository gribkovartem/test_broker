<?php

namespace App\Resources;

/**
 * Сообщения об ошибках
 * Class Messages
 * @package App\Resources
 */
class Messages
{
    const INCORRECT_LOGIN = 'Неправильный логин';
    const INCORRECT_PASSWORD = 'Неправильный пароль';
    const INCORRECT_DATA = 'Неправильные данные';
    const TOKEN_EXPIRED = 'Истекло время жизни токена';
    const NO_DATA = 'Не переданы данные';
    const NO_TIMESTAMP = 'Не указан таймстемп';
    const NO_CLIENT_ID = 'Не указан id клиента';
    const NOT_FOUND_CLIENT_ID = 'Не найден данный id клиента';
    const NO_INSTRUMENT_ID = 'Не указан id инструмента';
    const NOT_FOUND_INSTRUMENT_ID = 'Не найден данный id инструмента';
}